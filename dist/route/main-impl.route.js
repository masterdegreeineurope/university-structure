"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainImplRoute = void 0;
const express_1 = require("express");
class MainImplRoute {
    constructor(controller) {
        this.controller = controller;
        this.route = (0, express_1.Router)();
        this.route.post('/', this.controller.create),
            this.route.get('/', this.controller.getAll),
            this.route.get('/', this.controller.getById);
        this.route.put('/', this.controller.update),
            this.route.delete('/', this.controller.delete);
    }
}
exports.MainImplRoute = MainImplRoute;
//# sourceMappingURL=main-impl.route.js.map